#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <grp.h>
#include <pwd.h>
#include <time.h>

#define STDOUT 1
#define STDERR 2

#define MAX_PATH_LENGTH 4096

#define USAGE_SYNTAX "[OPTIONS] -i INPUT -o OUTPUT"
#define USAGE_PARAMS "OPTIONS:\n\
  -i, --input  INPUT_FILE  : input file\n\
  -o, --output OUTPUT_FILE : output file\n\
***\n\
  -v, --verbose : enable *verbose* mode\n\
  -h, --help    : display this help\n\
"


/**
 * Procedure which displays binary usage
 * by printing on stdout all available options
 *
 * \return void
 */
void print_usage(char* bin_name) {
  dprintf(1, "USAGE: %s %s\n\n%s\n", bin_name, USAGE_SYNTAX, USAGE_PARAMS);
}


/**
 * Procedure checks if variable must be free
 * (check: ptr != NULL)
 *
 * \param void* to_free pointer to an allocated mem
 * \see man 3 free
 * \return void
 */
void free_if_needed(void* to_free) {
  if (to_free != NULL) free(to_free);  
}


/**
 *
 * \see man 3 strndup
 * \see man 3 perror
 * \return
 */
char* dup_optarg_str() {
  char* str = NULL;

  if (optarg != NULL) {
    str = strndup(optarg, MAX_PATH_LENGTH);
    
    // Checking if ERRNO is set
    if (str == NULL) 
      perror(strerror(errno));
  }

  return str;
}


/**
 * Binary options declaration
 * (must end with {0,0,0,0})
 *
 * \see man 3 getopt_long or getopt
 * \see struct option definition
 */
static struct option binary_opts[] = 
{
  { "help",    no_argument,       0, 'h' },
  { "verbose", no_argument,       0, 'v' },
  { "input",   required_argument, 0, 'i' },
  { "output",  required_argument, 0, 'o' },
  { 0,         0,                 0,  0  } 
};

/**
 * Binary options string
 * (linked to optionn declaration)
 *
 * \see man 3 getopt_long or getopt
 */ 
const char* binary_optstr = "hvi:o:";



/**
 * Copy the behavior of ls command without options
 * \param src : path (directory or file)
 * \return success (0) or failure (-1)
 */
int ls_like_without_apt(char* src);

/**
 * Display property of file
 * \param src : file path
 * \return success (0) or failure (-1)
 */ 
int show_property_of_file(char* src);

/**
 * Display content of path
 * \param src : file path
 * \return success (0) or failure (-1)
 */ 
int show_content_of_path(char* src);

/**
 * Convert Mouth number to char
 * \param src : file path
 * \return success (0) or failure (-1)
 */ 
char* convertMount(int num);

/**
 * Binary main loop
 *
 * \return 1 if it exit successfully 
 */
int main(int argc, char** argv) {
  /**
   * Binary variables
   * (could be defined in a structure)
   */
  short int is_verbose_mode = 0;
  char* bin_input_param = NULL;
  char* bin_output_param = NULL;

  // Parsing options
  int opt = -1;
  int opt_idx = -1;

  while ((opt = getopt_long(argc, argv, binary_optstr, binary_opts, &opt_idx)) != -1) {
    switch (opt) {
      case 'i':
        //input param
        if (optarg) {
          bin_input_param = dup_optarg_str();     
        }
        break;
      case 'o':
        //output param
        if (optarg) {
          bin_output_param = dup_optarg_str();
        }
        break;
      case 'v':
        //verbose mode
        is_verbose_mode = 1;
        break;
      case 'h':
        print_usage(argv[0]);

        free_if_needed(bin_input_param);
        free_if_needed(bin_output_param);
 
        exit(EXIT_SUCCESS);
      default :
        break;
    }
  } 

  /**
   * Checking binary requirements
   * (could defined in a separate function)
   */
  if (bin_input_param == NULL || bin_output_param == NULL) {
    dprintf(STDERR, "Bad usage! See HELP [--help|-h]\n");

    // Freeing allocated data
    free_if_needed(bin_input_param);
    free_if_needed(bin_output_param);
    // Exiting with a failure ERROR CODE (== 1)
    exit(EXIT_FAILURE);
  }


  // Printing params
  dprintf(1, "** PARAMS **\n%-8s: %s\n%-8s: %s\n%-8s: %d\n", 
          "input",   bin_input_param, 
          "output",  bin_output_param,  
          "verbose", is_verbose_mode);

  // Business logic must be implemented at this point

  show_content_of_path(bin_input_param);
  

  // Freeing allocated data
  free_if_needed(bin_input_param);
  free_if_needed(bin_output_param);


  return EXIT_SUCCESS;
}


int show_property_of_file(char* src) {
  
  struct stat property;

  lstat(src, &property);

  time_t t = property.st_mtime;
  struct tm time;
  localtime_r(&t, &time);
  
  //char* strtime = (char*) calloc(100, sizeof(char));
  //strtime = ctime(&t);

  struct passwd *pw = getpwuid(property.st_uid);
  struct group *gr = getgrgid(property.st_gid);  
  
  if (pw == NULL){
    printf("Getting user info failed ! \n"); 
    perror(strerror(errno));
    return(-1);
  }

  if (gr == NULL) {
    printf("Getting group info failed ! \n"); 
    perror(strerror(errno));
    return(-1);
  }
  //printf( "%ld  ", property.st_ino);
  printf( (S_ISDIR(property.st_mode)) ? "d" : "-" );
  printf( (property.st_mode & S_IRUSR) ? "r" : "-" );
  printf( (property.st_mode & S_IWUSR) ? "w" : "-" );
  printf( (property.st_mode & S_IXUSR) ? "x" : "-" );
  printf( (property.st_mode & S_IRGRP) ? "r" : "-" );
  printf( (property.st_mode & S_IWGRP) ? "w" : "-" );
  printf( (property.st_mode & S_IXGRP) ? "x" : "-" );
  printf( (property.st_mode & S_IROTH) ? "r" : "-" );
  printf( (property.st_mode & S_IROTH) ? "w" : "-" );
  printf( (property.st_mode & S_IROTH) ? "x  " : "-  " );
  printf( "%s  "    , pw->pw_name );
  printf( "%s  "    , gr->gr_name );
  printf( "%lo  "   , property.st_size );
  printf( "%d  "    , 1900 + time.tm_year);

  char* mounth = convertMount(time.tm_mon);
  printf( "%s  "    , mounth);
  printf( "%d  "    , time.tm_mday);
  if(time.tm_hour < 10)
    printf( "0%d"   , time.tm_hour);
  else 
    printf( "%d"    , time.tm_hour);
  if(time.tm_min < 10)
    printf( ":0%d  ", time.tm_min);
  else 
    printf( ":%d  " , time.tm_min);

  printf( "%s", src );
  printf("\n");
  return 0;
}

int show_content_of_path(char* src) {

  DIR* directory = NULL; 
  struct dirent* directory_entity = NULL;
  

  // Open directory
  directory = opendir(src);
  if (directory == NULL) {

    int f_input = open(src, O_RDONLY);
    if (f_input == -1) {
      printf("Open input file failed ! \n");
      perror(strerror(errno));
      return(-1);
    }
    else{
      show_property_of_file(src);
      return(-1);
    }

  }
  
  printf("Path : %s/ \n", src);

  // Read content of directory
  while( (directory_entity = readdir(directory)) != NULL ) {
    
    // Get full path of file/directory
    char* path_copy = malloc(strlen(src)+ 1 + strlen(directory_entity->d_name)+1);
    strcpy(path_copy, src);
    strcat(path_copy, "/");
    strcat(path_copy, directory_entity->d_name);  

    show_property_of_file(directory_entity->d_name);
    free(path_copy);
  }
  
  // Close directory
  if (closedir(directory) == -1) {
    printf("Close directory file failed ! \n");
    perror(strerror(errno));
    return(-1);
  }

  return(0);
}

char* convertMount(int num) {
  char* mount = malloc(5 * sizeof(char));
  switch(num)
  {
    case 1: mount = "Jan"; break;
    case 2: mount = "Feb"; break;
    case 3: mount = "Mar"; break;
    case 4: mount = "Apr"; break;
    case 5: mount = "May"; break;
    case 6: mount = "Jun"; break;
    case 7: mount = "Jul"; break;
    case 8: mount = "Aug"; break;
    case 9: mount = "Sep"; break;
    case 10: mount = "Oct"; break;
    case 11: mount = "Nov"; break; 
  }
  return mount;
}
