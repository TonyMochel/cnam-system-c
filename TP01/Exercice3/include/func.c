#include "func.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>

int copy_file_to(char* src_input, char* src_output){

  int f_input, f_output;

  // open input file
  f_input = open(src_input, O_RDONLY);
  if (f_input == -1) {
    printf("Open input file failed ! \n");
    exit(-1);
  }
  
  // open output file (permission : rw-rw-r--)
  f_output = open(src_output, O_CREAT | O_WRONLY, 0664);
  if (f_output == -1) {
    printf("Open output file failed ! \n");
    close(f_input);
    exit(-1);
  }

  // define buffer stream
  char* buffer = (char*) calloc(100, sizeof(char));

  // start read input file
  int r_bytes = read(f_input, buffer, sizeof(buffer));

  // write content of input file to output file   
  while (r_bytes > 0) {
    write(f_output, buffer, r_bytes);
    r_bytes = read(f_input, buffer, sizeof(buffer));    
  }

  // close input file
  if (close(f_input) == -1) {
    printf("Cannot close input file ! \n");
    exit(-1);
  }

  // close output file
  if (close(f_output) == -1) {
    printf("Cannot close output file ! \n");
    exit(-1);
  }

  exit(0);
} 
int reverse_content_of_file(char* src){
  
  int f_input;

  // open input file
  f_input = open(src, O_RDONLY);
  if (f_input == -1) {
    printf("Open input file failed ! \n");
    exit(-1);
  }

  char buffer;
  int i, f_input_endpos = 0;

  // Get last position of file
  f_input_endpos = lseek(f_input, 0, SEEK_END);

  // print reverse content of input file to cout
  for(i = f_input_endpos-1; i >= 0; i--) {
    lseek (f_input, i, SEEK_SET);
    read (f_input, &buffer, sizeof(buffer));
    printf("%c", buffer);
  }

  // close input file
  if (close(f_input) == -1) {
    printf("Cannot close input file ! \n");
    exit(-1);
  }

  exit(0);
}
int ls_like_without_apt(char* src) {

  struct dirent* directory; 
  struct stat property;
  int f_input;

  //DIR* directory = opendir(source);

  f_input = open(src, O_RDONLY);
  if (f_input == -1) {
    // list property of files in the source directory
    
    printf("Open file failed ! \n");
  }
  else {
    printf("Open file succed ! \n");
    // list property of current file
    //struct stat f_input_property;
    //if (stat(source, &f_input_property == 0)) {
    // display_file_properties(f_input_property);
    //} else {      
    //  perror(strerror(errno));
    //}
  }
  
}