#include <stdio.h>
#include <stdlib.h>


#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <signal.h>

int signalexecution = 0;
unsigned int count = 0;

void signalHandler(int signal_no);


int main(int argc, char** argv) {
    
    sigset_t sig_proc;
    sigemptyset(&sig_proc);

    // signal ctrl+C
    struct sigaction action;
    action.sa_mask = sig_proc;
    action.sa_flags = 0;
    action.sa_handler = signalHandler;
    sigaction(SIGINT, &action, 0);
    sigaction(SIGTERM, &action, 0);

    printf("Programme sans fin. Utilisez ctrl+C pour le stoper\n");
    do {
        count++;
    } while (signalexecution == 0);

    exit(EXIT_SUCCESS);
}

void signalHandler(int signal_no) 
{
    switch(signal_no)
    {
        case SIGINT:
            printf("SIGINT : nombre %d \n", count);
            break;
        case SIGTERM: 
            printf("SIGTERM : Fin du programme !\n");
            break;
    }
    signalexecution = 1;
}
