#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <pthread.h>

#define SIZE (int)10
#define NB_THREAD 1
#define MAX_VALUE 50
/* Global Variables */

int tab[SIZE];
int min_tab[NB_THREAD];
int max_tab[NB_THREAD];
int thread_no = 0;

/* Prototypes */

void initialize_random_array(int* tab, int size);
void* search_min(void* args);
void* search_max(void* args);

// Main method
int main(int argc, char** argv) {
    
    // intialize array
    initialize_random_array(tab, SIZE);
        
    for(int index = 0; index < NB_THREAD; index++) {
        min_tab[index] = SIZE+1;
        max_tab[index] = 0;
    }
    
    // array of thread
    pthread_t min_threads[NB_THREAD];
    pthread_t max_threads[NB_THREAD];

    int index = 0;
    int mi_index = 0;
    int ma_index = 0;

    /* SEARCH MIN VALUES */
    
    // find min value
    for(mi_index = 0; mi_index < NB_THREAD; mi_index++) {
        pthread_create(&min_threads[mi_index], NULL, search_min, (void*)NULL);
    }
    // wait threads
    for(mi_index = 0; mi_index < NB_THREAD; mi_index++) {
        pthread_join(min_threads[mi_index], NULL);
    }
    

    /* SEARCH MAX VALUES */
    
    // find max value
    for(ma_index = 0; ma_index < NB_THREAD; ma_index++) {
        pthread_create(&max_threads[ma_index], NULL, search_max, (void*)NULL);
    }
    // wait threads
    for(ma_index = 0; ma_index < NB_THREAD; ma_index++) {
        pthread_join(max_threads[ma_index], NULL);
    }
    

    // display info
    for(index = 0; index < SIZE; index++) {
        printf("Value : %d\n", tab[index]);
    }
    for(mi_index = 0; mi_index < NB_THREAD; mi_index++) {
        printf("Minimum : %d\n", min_tab[mi_index]);
    }
    for(ma_index = 0; ma_index < NB_THREAD; ma_index++) {
        printf("Maximum : %d\n", max_tab[ma_index]);
    }

    int min_value = min_tab[0];
    int max_value = max_tab[0];
    for(index = 0; index < NB_THREAD-1; index++) {
        if(min_tab[index] < min_value)
        {
            min_value = min_tab[index];
        }
        if(max_tab[index] > max_value) {
            max_value = max_tab[index];
        }
    }

    printf("MINUMUM : %d \n", min_value);
    printf("MAXIMUM : %d \n", max_value);

    exit(EXIT_SUCCESS);
}


void initialize_random_array(int* tab, int size) {
    srand(time(NULL));

    for (int index=0; index<size; index++) {
        tab[index] = rand() % MAX_VALUE;
    }
}
void* search_min(void* args){

    int offset = thread_no++;
    int binf = offset*(SIZE/NB_THREAD);
    int bsup = (offset+1)*(SIZE/NB_THREAD);

    int min = MAX_VALUE+1;

    for (int index=binf; index<bsup; index++) {
        if(tab[index] < min) {
            min = tab[index];
        }
    }
    min_tab[offset] = min;
    pthread_exit(EXIT_SUCCESS);
}
void* search_max(void* args){
    int offset = thread_no++;
    int binf = offset*(SIZE/NB_THREAD);
    int bsup = (offset+1)*(SIZE/NB_THREAD);

    int max = 0;

    for (int index=binf; index<bsup; index++) {
        if(tab[index] > max) {
            max = tab[index];
        }
    }

    printf("max : %d\n", max);
    max_tab[offset] = max;
    pthread_exit(EXIT_SUCCESS);
}


