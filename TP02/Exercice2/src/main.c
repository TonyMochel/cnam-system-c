#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char** argv) {
    
    pid_t process = fork();

    // NOTE : Au lieu de générer un fichier temporaire dans le répértoir "/temp" nous avons défini un dossier spécifique "./temp" pour l'exercice. 

    // define temporary file
    char temp_path[] = "./tmp/proc-exercise-XXXXXX";

    if (process == 0) 
    {
        int closed = close(1);   // close stdout
        if (closed == -1) {
            perror("Close stdout fail");
            exit(EXIT_FAILURE);
        }

        printf("Child : pid %d \n", getpid());

        // create temporary file
        int file_descriptor = mkstemp(temp_path);

        if (file_descriptor == -1) {
            perror("Error - Invalide temporary path (mkstemp) ! \n");
            exit(EXIT_FAILURE);
        }
    
        int duplicate = dup(file_descriptor);
        if (duplicate == -1) {
            perror("Error duplication of temporary's fd ! \n");
            exit(EXIT_FAILURE);
        }
        printf("descriptor : %d - duplicate %d \n", file_descriptor, duplicate);
        
        // execute affichage program
        execl("./Afficher/bin/Affichage", "./Afficher/bin/Affichage", "Toto", (char*) NULL);       
        
        exit(EXIT_SUCCESS);
    }
    else 
    {
        printf("Father : pid %d \n", getpid());
        wait(&process);
        printf("That's All Forks ! \n");
    }
    exit(EXIT_SUCCESS);
}

// Close 1 : printf is copied to temporary file
// Close 2 : printf is copied to stdout

